    #!/bin/bash

paint() {
    local norm="$(printf '\033[0m')"
    local bold="$(printf '\033[1m')"
    sed -e "s/.*/${bold}&${norm}/"
}

do_packs() {
    echo -e "UPDATE: Packages index ..." | paint
    sudo aptitude -q=2 update
    echo -e "UPDATE: Packages content ..." | paint
    sudo apt-file update 1>/dev/null
    echo -e "UPGRADE: All installed packages ..." | paint
    sudo aptitude upgrade
    echo -e "REMOVE: Unneeded packages ..." | paint
    sudo aptitude -q=2 -o Aptitude::Delete-Unused=1 install
    echo -e "REMOVE: Old configurations ..." | paint
    sudo aptitude -q=2 purge ~c
    echo -e "CLEAN: Local packages cache ..." | paint
    sudo aptitude -q=2 clean
}

sudo -v
do_packs

