#!/bin/bash

upgradable=$(apt list --upgradable 2>/dev/null | wc -l)
onhold=$(dpkg --get-selections | grep -c 'hold$')

newsup=$((upgradable - onhold))

hostnm=$(hostname -f)
rcptto="${1}"

if [ "${newsup}" -gt 0 ] ; then
    cat <<EOF | mail -s Updates "${rcptto}"

Host:   ${hostnm}

You have ${newsup} package(s) that can be upgraded.

EOF
fi
